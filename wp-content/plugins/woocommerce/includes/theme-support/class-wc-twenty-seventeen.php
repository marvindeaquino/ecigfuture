<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Twenty Seventeen suport.
 *
 * @class   WC_Twenty_Seventeen
 * @since   2.6.9
 * @version 2.6.9
 * @package WooCommerce/Classes
 */
class WC_Twenty_Seventeen {

	/**
	 * Theme init.
	 */
	public static function init() {
		remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
		remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

		add_action( 'woocommerce_before_main_content', array( __CLASS__, 'output_content_wrapper' ), 10 );
		add_action( 'woocommerce_after_main_content', array( __CLASS__, 'output_content_wrapper_end' ), 10 );
		add_filter( 'woocommerce_enqueue_styles', array( __CLASS__, 'enqueue_styles' ) );
	}

	/**
	 * Enqueue CSS for this theme.
	 *
	 * @param  array $styles
	 * @return array
	 */
	public static function enqueue_styles( $styles ) {
		unset( $styles['woocommerce-general'] );

		$styles['woocommerce-twenty-seventeen'] = array(
			'src'     => str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/css/twenty-seventeen.css',
			'deps'    => '',
			'version' => WC_VERSION,
			'media'   => 'all',
		);

		return apply_filters( 'woocommerce_twenty_seventeen_styles', $styles );
	}

	/**
	 * Open the Twenty Seventeen wrapper.
	 */
	public static function output_content_wrapper() { ?>
        <?php
            global $wp_query;
                   $cat = $wp_query->get_queried_object();
        ?>
        <div class="product-page-<?php echo $cat->parent==0?'category':'subcategory';?>">
            <div class="single-product-page">
                <div class="container">
                    <div class="row">
                        <?php if(is_single(single-product)):?>
                        <div class="col-md-8">
                        <?php else:?>
                            <div class="col-lg-<?php echo $cat->parent==0?'12':'8 col-md-8'; ?>">
                        <?php endif;?>
                            <main id="main" class="site-main" role="main">
		<?php
	}

	/**
	 * Close the Twenty Seventeen wrapper.
	 */
	public static function output_content_wrapper_end() { ?>
                            </main>
                        </div>
                        <?php
                            global $wp_query;
                                   $cat = $wp_query->get_queried_object();
                        ?>
                        <?php if(is_single(single-product) || $cat->parent>0):?>
                        <div class="col-lg-4 col-md-4">
                            <?php get_sidebar('single-product'); ?>
                        </div>

                        <?php endif;?>
                    </div>
                </div>
            </div><!-- end of single-product-page-->
        </div><!-- end of category or subcategory-->
		<?php
	}
}

WC_Twenty_Seventeen::init();
