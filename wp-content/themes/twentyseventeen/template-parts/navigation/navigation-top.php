<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav class="navbar navbar-default navbar-transparent">
    <div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a class="navbar-brand navbar-lg" href="/"><img src="<?php /*echo get_template_directory_uri();*/?>/assets/images/ecig-future.png" alt="Ecig Future"></a>-->
            <?php theme_prefix_the_custom_logo()?>

        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php wp_nav_menu( array(
                'menu' => 'Primary Menu',
                'menu_id'        => 'hover-menu',
                'menu_class'     => 'nav navbar-nav navbar-left',
                'container'      => ''
            ) ); ?>

            <?php wp_nav_menu( array(
                'menu' => 'Primary Menu B',
                'menu_id'        => 'hover-menu',
                'menu_class'     => 'nav navbar-nav navbar-right',
                'container'      => ''
            ) ); ?>

          <!--   <?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && has_custom_header() ) : ?>
                <a href="#content" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'twentyseventeen' ); ?></span></a>
            <?php endif; ?> -->
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</nav>

