<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="section section-banner">
    <div id="first-slider">
        <div id="carousel-example-generic" class="carousel slide carousel-fade">
            <!-- Indicators -->
            <ol class="carousel-indicators">

                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>

            </ol>
            <div class="carousel-control-container">
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <i class="carousel-prev"></i><span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <i class="carousel-next"></i><span class="sr-only">Next</span>
                </a>
                <div class="clearfix"></div>
            </div>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php if( have_rows('home_banner_slider') ): ?>
                <?php while( have_rows('home_banner_slider') ): the_row();
                    $slider_num = get_sub_field('slider_item_number');
                    $slider_img = get_sub_field('slider_image');
                    $slider_title = get_sub_field('slider_title');
                    $slider_details = get_sub_field('slider_details');
                    $slider_link = get_sub_field('slider_button_link');
                    $slider_btn_txt = get_sub_field('slider_button_text');
                    $slider_btn_cname = get_sub_field('slider_button_classname');
                ?>
                <!-- Item 1 -->
                <div class="item <?php if($slider_num == 1){echo 'slide'.$slider_num.' '.'active';}else{echo 'slide'.$slider_num;}?>" style="background-image: url('<?php echo $slider_img; ?>');">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="slider-content text-center">
                                    <h3 class="slider-title" data-animation="animated bounceInDown"><?php echo $slider_title?></h3>
                                    <p class="slider-details" data-animation="animated bounceInUp"><?php echo $slider_details?></p>
                                    <a class="btn btn-lg btn-red" href="<?php echo $slider_link?>"><?php echo $slider_btn_txt?></a>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
                <?php  endwhile; ?>
                <?php endif;
                ?>

            </div>
            <!-- End Wrapper for slides-->
        </div>
    </div>
    <div class="section-banner-footer bg-light-dark">
        <div class="container">
            <?php echo the_field('promo_sale')?>
        </div>
    </div>
</div>
<section class="section section-product-container bg-gray">
    <div class="container">
        <div class="section-product-items">
            <div class="row">
                <?php if( have_rows('product_category_section') ): ?>
                    <?php while( have_rows('product_category_section') ): the_row();
                    $product_category_img = get_sub_field('product_category_image');
                    $product_category_button_text = get_sub_field('product_category_button_text');
                    $product_category_link = get_sub_field('product_category_link');
                    $product_category_button = get_sub_field('product_category_button');
                    ?>
                <div class="col-sm-6 col-md-3">
                    <div class="product-item-container">
                        <div class="product-item-img">
                            <img src="<?php echo $product_category_img ?>">
                        </div>
                        <div class="product-item-content">
                            <a class="btn btn-lg active" href="<?php echo $product_category_link?>"><?php echo $product_category_button_text?></a>
                            <a class="shop-now-link" href="<?php echo $product_category_link?>">Shop now</a>
                        </div>
                    </div>
                </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <span class="product-link-lower" id="spnbottom"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon/arrow-head.png"></span>
</section>
<section class="section section-ecigarette">
    <div class="section-ecigarrete-container">
        <div class="container">
            <div class="row">
                <?php if( have_rows('ecigarette_section_guide') ): ?>
                    <?php while( have_rows('ecigarette_section_guide') ): the_row();
                    $ecigarrete_section_guide_background = get_sub_field('ecigarrete_section_guide_background');
                    $ecigarrete_section_guide_title = get_sub_field('ecigarrete_section_guide_title');
                    $ecigarrete_section_guide_text_link = get_sub_field('ecigarrete_section_guide_text-link');
                    $ecigarrete_section_guide_link = get_sub_field('ecigarrete_section_guide_link');
                    ?>
                <div class="col-sm-4 col-md-4">
                    <div class="ecigarrete-container">
                        <img src="<?php echo $ecigarrete_section_guide_background?>" alt="">
                        <div class="ecigarrete-content">
                            <h3><?php echo $ecigarrete_section_guide_title?></h3>
                            <a class="ecigarrete-link" href="<?php echo $ecigarrete_section_guide_link?>"><?php echo $ecigarrete_section_guide_text_link?><i class="arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                    <?php  endwhile; ?>
                <?php endif;
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-promotions">
    <div class="container">
        <div class="promotion-content">
            <div class="promotion-head">
                <p class="promotion-context">
                    <span class=""><?php echo the_field('promotion_head_section_zero')?></span>
                    <h3 class="section-title"><?php echo the_field('promotion_head_section_first')?></h3>
                </p>
                <ul class="list-inline panel-tabs">
                    <?php if(have_rows('promotion_head_section_second')):?>
                        <?php while(have_rows('promotion_head_section_second')):the_row();?>
                            <?php
                                $promotion_head_promo_type = get_sub_field('promotion_head_promo_type');
                                $promotion_head_promo_type_link	 = get_sub_field('promotion_head_promo_type_link');
                            ?>
                            <li class="<?php if($promotion_head_promo_type_link == 'tab1')echo 'active'?>"><a href="#<?php echo $promotion_head_promo_type_link?>" data-toggle="tab"><?php echo $promotion_head_promo_type?></a></li>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>

            <div class="promotion-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="promotional-items">
                            <ul class="featured-product-home-marvin products">
                                <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_visibility',
                                            'field'    => 'name',
                                            'terms'    => 'featured',
                                        ),
                                    ),
                                );
                                $loop = new WP_Query( $args );

                                if ( $loop->have_posts() ) {
                                    $i = 1;
                                    while ( $loop->have_posts() ) : $loop->the_post();?>


                                        <?php
                                        echo "<div class='col-xs-12 col-sm-6 col-md-3 team_columns_item_image'>".$x;
                                        wc_get_template_part( 'content', 'product' );
                                        echo "</div>";
                                    endwhile;
                                } else {
                                    echo __( 'No products found' );
                                }
                                wp_reset_postdata();
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <div class="promotional-items">
                            <ul class="featured-product-home-marvin products">
                                <?php
                                $args = array(
                                    'post_type' => 'product',
                                    'meta_key' => 'total_sales',
                                    'orderby' => 'meta_value_num',
                                );
                                $loop = new WP_Query( $args );

                                if ( $loop->have_posts() ) {
                                    while ( $loop->have_posts() ) : $loop->the_post();?>
                                        <?php
                                        echo "<div class='col-xs-12 col-sm-6 col-md-3 team_columns_item_image'>";
                                        wc_get_template_part( 'content', 'product' );
                                        echo "</div>";
                                    endwhile;
                                } else {
                                    echo __( 'No products found' );
                                }
                                wp_reset_postdata();
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>





    </div>
</section>
<section class="section section-subscribe text-center"
         style="background-image: url(<?php echo the_field('section_subscribe_background') ?>)">
    <div class="container">
        <div class="subscribe-container">
            <p class="subscribe-content">
                <?php echo the_field('section_subscribe_content')?>
            </p>
            <a href="<?php the_field('section_subscribe_button_link')?>" class="btn btn-red">Subscribe now!</a>
        </div>
    </div>
</section>
<div class="section section-home-details bg-gray">
    <div class="container">
        <div class="home-details-container">
            <h2 class="title"><?php echo the_field('section_home_details_title')?></h2>
            <p class="details"><?php echo the_field('section_home_details_about')?></p>
        </div>
    </div>
</div>


