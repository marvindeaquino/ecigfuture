<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<div id="page" class="site">

    <div class="top-header bg-dark">
        <div class="container">
            <div class="section section-login">
                <?php if(is_user_logged_in()):?>
                    <ul class="list-inline pull-right reset-mp">
                        <li>
                            <form role="search" method="get" class="header-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
                                <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="form-control input-sm" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
                                <button type="submit" role="button" class="btn-srch-2"><i class="fa fa-search"></i></button>
                                <button type="button" role="button" class="btn-srch"><i class="fa fa-search"></i></button>
                                <input type="hidden" name="post_type" value="product" />
                            </form>
                        </li>
                        <li><a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i><i class="icon icon-badge"><?php echo WC()->cart->get_cart_contents_count(); ?></i></a></li>
                        <li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><i class="icon icon-profile"></i>My account</a></li>
                    </ul>
                    <?php else:?>
                    <ul class="list-inline pull-right reset-mp">
                        <li>
                            <form role="search" method="get" class="header-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
                                <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="form-control input-sm" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
                                <button type="submit" role="button" class="btn-srch-2"><i class="fa fa-search"></i></button>
                                <button type="button" role="button" class="btn-srch"><i class="fa fa-search"></i></button>
                                <input type="hidden" name="post_type" value="product" />
                            </form>
                        </li>
                        <li><a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i><i class="icon icon-badge"><?php echo WC()->cart->get_cart_contents_count(); ?></i></a></li>
                        <li><a href="<?php echo get_permalink( 78 ); ?>">Login</a></li>
                    </ul>
                <?php endif;?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <header>
        <div class="container">
            <?php if (has_nav_menu('top')) : ?>
                <?php get_template_part('template-parts/navigation/navigation', 'top'); ?>
            <?php endif; ?>
        </div><!--end container -->
    </header>
    <?php

    /*
     * If a regular post or page, and not the front page, show the featured image.
     * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
     */
    if ((is_single() || (is_page() && !is_page(80) && !twentyseventeen_is_frontpage())) && has_post_thumbnail(get_queried_object_id())) :?>

        <?php
        $featured_img_url = get_the_post_thumbnail_url(get_queried_object_id(), 'twentyseventeen-featured-image');
        ?>

        <div class="single-featured-image-header"
             style="background-image:url(<?php echo esc_url($featured_img_url); ?>)">
            <div class="container">
                <div class="subpage-banner-content">
                    <?php if(get_field('subpages_title')): ?>
                        <h2 class="subpage-banner-title"><?php echo the_field('subpages_title'); ?> </h2>
                    <?php else: ?>
                        <h2 class="subpage-banner-title">Blog</h2>
                    <?php endif; ?>

                    <?php custom_breadcrumbs(); ?>
                </div>
            </div>
        </div><!-- .single-featured-image-header -->
    <?php elseif(is_page(78)): ?>
        <div class="registration-banner">
            <div class="container">
                <div class="registration-banner-content">
                    <h2 class="registration-banner-title"><?php the_field('subpages_title') ?></h2>
                    <?php woocommerce_breadcrumb(); ?>
                </div>
            </div>
        </div><!-- .single-featured-image-header -->

    <?php elseif(is_page(80)): ?>
        <div class="registration-banner">
            <div class="container">
                <div class="registration-banner-content">
                    <h2 class="registration-banner-title"><?php the_field('subpages_title') ?></h2>
                    <?php custom_breadcrumbs(); ?>
                </div>
            </div>
        </div><!-- .single-featured-image-header -->
    <?php elseif(is_page(76)): ?>
        <div class="registration-banner">
            <div class="container">
                <div class="registration-banner-content">
                    <h2 class="registration-banner-title"><?php the_field('subpages_title') ?></h2>
                    <?php custom_breadcrumbs(); ?>
                </div>
            </div>
        </div><!-- .single-featured-image-header -->

    <?php elseif(is_page(172)): ?>
    <div class="registration-banner">
        <div class="container">
            <div class="registration-banner-content">
                <h2 class="registration-banner-title"><?php the_field('subpages_title') ?></h2>
                <?php custom_breadcrumbs(); ?>
            </div>
        </div>
    </div><!-- .single-featured-image-header -->

    <?php elseif(is_search()): ?>
        <div class="registration-banner">
            <div class="container">
                <div class="registration-banner-content">
                    <h2 class="registration-banner-title">Search Result: <?php echo get_search_query(); ?></h2>
                    <?php custom_breadcrumbs(); ?>
                </div>
            </div>
        </div><!-- .single-featured-image-header -->

    <?php elseif(is_product_tag()): ?>
        <div class="registration-banner">
            <div class="container">
                <div class="registration-banner-content">
                    <h2 class="registration-banner-title">Search Result: <?php echo get_search_query(); ?></h2>
                    <?php woocommerce_breadcrumb(); ?>
                </div>
            </div>
        </div><!-- .single-featured-image-header -->

    <?php endif; ?>

    <div class="site-content-contain">
        <div>
            <!-- Woocommerce Category, Subcategory, Single Product Page Banner-->
            <?php
             if(is_product()):?>
                <div class="single-product-banner">
                    <div class="container">
                        <div class="single-product-banner-container">
                            <h1 class="title"><?php echo the_title();?></h1>
                            <?php woocommerce_breadcrumb(); ?>
                        </div>
                    </div>
                </div>

                <?php else:
                global $wp_query;
                $cat = $wp_query->get_queried_object();

                if ( is_product_category() ){
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
                    $image = wp_get_attachment_url( $thumbnail_id);
                    if ( $image ) {?>
                        <!--echo '<img src="' . $image . '" alt="' . $cat->name . '" />';-->
                        <div class="single-featured-image-header" style="background-image:url(<?php echo $image;?>)">
                            <div class="container">
                                <div class="subpage-banner-content">
                                    <?php if(get_field('subpages_title')): ?>
                                        <h2 class="subpage-banner-title"><?php echo the_field('subpages_title'); ?> </h2>
                                    <?php else: ?>
                                        <h2 class="subpage-banner-title"><?php echo $cat->name;?></h2>
                                    <?php endif; ?>
                                    <?php woocommerce_breadcrumb(); ?>
                                </div>
                            </div>
                        </div><?php
                    }
                }
             endif;?>

