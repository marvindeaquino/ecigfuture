<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $product;
if(is_front_page()){
    echo '<p class="short-excerpt">'.excerpt(10).'</p><!--excerpt for related products-->';
}
elseif(is_post_type_archive (post-type-archive-product)){
    echo '<p class="short-excerpt">'.excerpt(20).'</p><!--excerpt for related products-->';
}
else{
    echo '<p class="short-excerpt">'.excerpt(20).'</p><!--excerpt for related products-->';
}
if ( $product->product_type == "simple" ) {
    $simpleURL = get_permalink();
    $simpleLabel =  "SHOP NOW";  // BUTTON LABEL HERE
} else {
    $simpleURL =  $product->add_to_cart_url();  
    $simpleLabel = $product->add_to_cart_text();
};

echo apply_filters( 'woocommerce_loop_add_to_cart_link',
    sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_variable">shop now</a>',
        esc_url( $simpleURL ),
        esc_attr( $product->id ),
        esc_attr( $product->get_sku() ),
        esc_attr( isset( $quantity ) ? $quantity : 1 ),
        $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
        esc_attr( $product->product_type ),
        esc_html( $simpleLabel )
    ),
$product );
echo "</div><!--End related-product-content-->";
