<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>
<?php if (is_page(80)): ?>
    <?php wc_print_notices(); ?>

    <?php do_action('woocommerce_before_customer_login_form'); ?>
    <?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="registration-form-container" id="customer_login">
                    <h2 class="personal-information"><?php _e('Personal Information', 'woocommerce'); ?></h2>
                    <form method="post" class="register">

                        <?php do_action('woocommerce_register_form_start'); ?>

                        <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="reg_username"><?php _e('Username', 'woocommerce'); ?> <span
                                        class="required">*</span></label>
                                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                       name="username" id="reg_username"
                                       value="<?php echo (!empty($_POST['username'])) ? esc_attr($_POST['username']) : ''; ?>"/>
                            </p>

                        <?php endif; ?>
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="reg_email"><?php _e('Email address', 'woocommerce'); ?> <span
                                    class="required">*</span></label>
                            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                   name="email" id="reg_email"
                                   value="<?php echo (!empty($_POST['email'])) ? esc_attr($_POST['email']) : ''; ?>"/>
                        </p>

                        <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>
                            <h2 class="login-information">Login information</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide"
                                       id="password-inline">
                                        <label for="reg_password"><?php _e('Password', 'woocommerce'); ?> <span
                                                class="required">*</span></label>
                                        <input type="password"
                                               class="woocommerce-Input woocommerce-Input--text input-text"
                                               name="password" id="reg_password"/>
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="form-row form-row-wide" id="password-inline">
                                        <label for="reg_password2"><?php _e('Confirm password', 'woocommerce'); ?> <span
                                                class="required">*</span></label>
                                        <input type="password" class="input-text" name="password2" id="reg_password2"
                                               value="<?php if (!empty($_POST['password2'])) echo esc_attr($_POST['password2']); ?>"/>
                                    </p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <!-- Spam Trap -->
                        <div style="<?php echo((is_rtl()) ? 'right' : 'left'); ?>: -999em; position: absolute;"><label
                                for="trap"><?php _e('Anti-spam', 'woocommerce'); ?></label><input type="text"
                                                                                                  name="email_2"
                                                                                                  id="trap"
                                                                                                  tabindex="-1"
                                                                                                  autocomplete="off"/>
                        </div>

                        <?php do_action('woocommerce_register_form'); ?>

                        <p class="woocomerce-FormRow form-row">
                            <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                            <input type="submit" class="woocommerce-Button button register-btn" name="register"
                                   value="<?php esc_attr_e('Submit', 'woocommerce'); ?>"/>
                        </p>

                        <?php do_action('woocommerce_register_form_end'); ?>

                    </form>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>


    <?php endif; ?>

    <?php do_action('woocommerce_after_customer_login_form'); ?>


<?php else: ?>
    <?php wc_print_notices(); ?>

    <?php do_action('woocommerce_before_customer_login_form'); ?>

    <?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>

        <div class="u-columns col2-set" id="customer_login">

        <div class="u-column1 col-1">

    <?php endif; ?>

    <h2><?php _e('Returning Customers', 'woocommerce'); ?></h2>
    <p>If you've purchased from us before, please login with your email address and password.</p>

    <form class="woocomerce-form woocommerce-form-login login" method="post">

        <?php do_action('woocommerce_login_form_start'); ?>

        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="username"><?php _e('Username or email address', 'woocommerce'); ?> <span
                    class="required">*</span></label>
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
                   id="username"
                   value="<?php echo (!empty($_POST['username'])) ? esc_attr($_POST['username']) : ''; ?>"/>
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" id="password-p">
            <label for="password"><?php _e('Password', 'woocommerce'); ?> <span class="required">*</span></label>
            <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password"
                   id="password"/>
        </p>

        <?php do_action('woocommerce_login_form'); ?>

        <p class="woocommerce-LostPassword lost_password">
            <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php _e('Forgot your password?', 'woocommerce'); ?></a>
        </p>
        <p class="form-row">
            <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
            <input type="submit" class="woocommerce-Button button" name="login"
                   value="<?php esc_attr_e('Login', 'woocommerce'); ?>"/>
        </p>


        <?php do_action('woocommerce_login_form_end'); ?>

    </form>

    <?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>

        </div>

        <div class="u-column2 col-2">

            <h2 class="new-customer-title">New customer</h2>
            <p class="new-customer-meta">By creating an account with our store, you will be able to move through the checkout process faster,
                store multiple shipping addresses, view and track your orders in your account and more.</p>
            <div class="new-customer-btn">
                <a href="/create-account" class="btn-create-account">Create an account</a>
                <!--<a href="/create-account" class="btn-become-reseller">Become a reseller</a>-->
            </div>
        </div>

        </div>
    <?php endif; ?>

    <?php do_action('woocommerce_after_customer_login_form'); ?>
<?php endif; ?>




