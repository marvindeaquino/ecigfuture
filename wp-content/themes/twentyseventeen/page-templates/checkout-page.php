<?php
/**
 * Template Name: Checkout Page Template
 */

get_header(); // Loads the header.php template. ?>
    <div class="section-checkout-template">
        <div class="container">
            <div class="checkout-page-container">
                <?php
                // TO SHOW THE PAGE CONTENTS
                while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <div class="entry-content-page">
                        <?php the_content(); ?> <!-- Page Content -->
                    </div><!-- .entry-content-page -->

                    <?php
                endwhile; //resetting the page loop
                wp_reset_query(); //resetting the page query
                ?>
            </div>
        </div>
    </div>
<?php get_footer(); // Loads the footer.php template. ?>