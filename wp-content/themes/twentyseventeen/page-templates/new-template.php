<?php
/**
 * Template Name: New Template
 */

get_header(); // Loads the header.php template. ?>
    <div class="page-template-container">
        <div class="container">
            <div class="new-template-container">
                <?php
                // TO SHOW THE PAGE CONTENTS
                while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                        <?php the_content(); ?> <!-- Page Content -->
                    <?php
                endwhile; //resetting the page loop
                wp_reset_query(); //resetting the page query
                ?>
                <?php if( have_rows('accordion_repeater') ): ?>
                    <?php $scnt=1; while( have_rows('accordion_repeater') ): the_row();
                        $header_title = get_sub_field('header_title');
                        ?>
                        <h2><?php echo $header_title;?></h2>
                        <?php if( have_rows('accordion_inside_repeater') ): ?>
                            <div class="panel-group" id="accordion-<?php echo $scnt?>" role="tablist" aria-multiselectable="true">
                                <?php $cnt= 1; while( have_rows('accordion_inside_repeater') ): the_row();
                                    $accordion_title = get_sub_field('accordion_title');
                                    $accordion_content = get_sub_field('accordion_content');
                                    ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading-<?php echo $cnt.'-'.$scnt;?>">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion-<?php echo $scnt?>" href="#collapse-<?php echo $cnt.'-'.$scnt;?>" aria-expanded="true" aria-controls="collapse-<?php echo $cnt.'-'.$scnt;?>">
                                                    <i class="more-less glyphicon <?php echo $cnt==1?' glyphicon-minus':'glyphicon-plus'; ?>"></i>
                                                    <?php echo $accordion_title;?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse-<?php echo $cnt.'-'.$scnt;?>" class="panel-collapse collapse <?php if($cnt==1){echo 'in';}?>" role="tabpanel" aria-labelledby="heading-<?php echo $cnt.'-'.$scnt;?>">
                                            <div class="panel-body">
                                                <?php echo $accordion_content;?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php  $cnt++; endwhile; ?>

                            </div><!-- panel-group -->
                        <?php endif; ?>
                    <?php  $scnt++; endwhile; ?>
                <?php endif; ?>


            </div>
        </div>
    </div>
<?php get_footer(); // Loads the footer.php template. ?>