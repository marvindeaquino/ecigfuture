<?php
/**
 * Template Name: Spare parts
 */

get_header(); // Loads the header.php template. ?>
    <div class="page-template-container">
        <div class="container">
            <div class="about-us-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8" style="float: right;">
                        <header class="woocommerce-products-header">
                            <div class="product-cat-details">
                                 <?php the_title( '<h1>', '</h1>' ); ?>
                            </div>
                            
                        </header>
                        <?php
                        // TO SHOW THE PAGE CONTENTS
                        while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                            <div class="entry-content-page">
                                <?php the_content(); ?> <!-- Page Content -->
                            </div><!-- .entry-content-page -->

                            <?php
                        endwhile; //resetting the page loop
                        wp_reset_query(); //resetting the page query
                        ?>

                    </div>
                    <div class="col-md-4">
                       <?php dynamic_sidebar( 'sidebar-4' ); ?>
                    </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); // Loads the footer.php template. ?>