<?php
/**
 * Template Name: Contact Us
 */

get_header(); // Loads the header.php template. ?>
<div class="section-contact-us bg-gray">
    <div class="container">
        <div class="contact-us-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-us-container-left">
                        <h3 class="contact-us-left-title">Send us a message</h3>
                        <?php echo do_shortcode('[contact-form-7 id="115" title="Contact form 1"]')?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact-us-right-details">
                        <h3 class="contact-us-right-title">Contact info</h3>
                        <div class="contact-us-right-subdetails">
                            <?php echo the_field('contact_us_details')?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); // Loads the footer.php template. ?>