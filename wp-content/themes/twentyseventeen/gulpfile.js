var elixir = require('laravel-elixir');

elixir((mix) => {
	mix
	.sass('main.scss', 'assets/styles/main.css', 'assets/scss')
	.scripts([
		'assets/scripts/back_to_top.js',
		'assets/scripts/append.js',
		'assets/scripts/input-number.js',
		'assets/scripts/search-box.js',
		'assets/scripts/slick.min.js',
		'assets/scripts/feature-slider.js',
		'assets/scripts/navigation-dropdown.js',
		'assets/scripts/accordion.js',
	], 'assets/scripts/all.js', './')
});
