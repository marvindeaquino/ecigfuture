<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="single-post">
    <div class="container">
        <div class="single-post-container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post();

                        get_template_part( 'template-parts/post/content', get_post_format() );

                        // If comments are open or we have at least one comment, load up the comment template.
                        /*if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;*/

                        the_post_navigation( array(
                            /*'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
                            'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',*/

                            'prev_text' => '<span class="icon-controler-container"><i class="icon-prev-post"></i></span><span class="nav-subtitle">' . __( 'Prev Article', 'twentyseventeen' ) . '</span>',
                            'next_text' => '<span class="nav-subtitle">' . __( 'Next Article', 'twentyseventeen' ) . '</span><span class="icon-controler-container"><i class="icon-next-post"></i></span>',
                        ) );

                    endwhile; // End of the loop.
                    ?>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .single-post-container -->

    </div><!-- .container -->
</div><!-- .single-post -->
<?php get_footer();
