//Return to top
$(document).ready(function(){
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});

//Go to ecigarrete sections
$('#spnbottom').on("click",function() {
    var percentageToScroll = 100;
    var height = jQuery(document).innerHeight();
    var documentHeight = $(document).height();
    var scrollAmount = Math.round(height * percentageToScroll/ 300);
    // alert(scrollAmount);
    var overheight = jQuery(document).height() - jQuery(window).height();
    //alert(overheight);
    jQuery("html, body").animate({scrollTop: scrollAmount}, 900);
});
