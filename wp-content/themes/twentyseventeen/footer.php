<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

        </div><!-- #content -->

        <footer class="section section-footer bg-dark">
            <div class="container">
                <div class="footer-container">
                    <div class="row">
                        <!--<div class="col-md-3 col-md-3-new">
                            <h3 class="title">Contact us</h3>
                            <ul class="list-unstyled">
                                <li class="details"><i class="fa fa-phone" aria-hidden="true"></i><span class="contact-label">Phone:</span><span class="number"><?php /*echo the_field('phone_number', 'option')*/?></span></li>
                                <li class="details"><i class="fa fa-envelope" aria-hidden="true"></i><span class="contact-label">Email:</span><span class="email"><?php /*echo the_field('email_address', 'option')*/?></span></li>
                            </ul>

                            <div class="social-media-links">
                                <h3 class="title">Stay connected</h3>
                                <ul class="list-inline">
                                    <li><span class="icon-social-link-container"><a href="<?php /*echo the_field('facebook_link', 'option');*/?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></span></li>
                                    <li><span class="icon-social-link-container"><a href="<?php /*echo the_field('twitter_link', 'option');*/?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></span></li>
                                    <li><span class="icon-social-link-container"><a href="<?php /*echo the_field('youtube_link', 'option');*/?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></span></li>
                                    <li><span class="icon-social-link-container"><a href="<?php /*echo the_field('googleplus_link', 'option');*/?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-md-2-new">
                            <h3 class="title">Legal</h3>
                            <?php /*wp_nav_menu( array(
                                'menu' => 'Legal',
                                'menu_id'        => '',
                                'menu_class'     => 'list-unstyled',
                                'container'      => ''
                            ) ); */?>
                        </div>
                        <div class="col-md-2 col-md-2-new-2">
                            <h3 class="title">Quick link</h3>
                            <?php /*wp_nav_menu( array(
                                'menu' => 'Footer Quick Link',
                                'menu_id'        => '',
                                'menu_class'     => 'list-unstyled',
                                'container'      => ''
                            ) ); */?>
                        </div>
                        <div class="col-md-2 col-md-2-new">
                            <h3 class="title">My account</h3>
                            <?php /*wp_nav_menu( array(
                                'menu' => 'Footer My Account',
                                'menu_id'        => '',
                                'menu_class'     => 'list-unstyled',
                                'container'      => ''
                            ) ); */?>
                        </div>
                        <div class="col-md-4 col-md-4-new">
                            <h3 class="title">Shipping services</h3>
                            <a href=""><img src="<?php /*echo the_field('australia_post','option')*/?>"></a>
                            <ul class="list-inline">
                                <li><a href=""><img src="<?php /*echo the_field('post_express_delivery','option')*/?>"></a></li>
                                <li><a href=""><img src="<?php /*echo the_field('australia_map','option')*/?>"></a></li>
                                <li><a href=""><img src="<?php /*echo the_field('secured_services','option')*/?>"></a></li>
                                <li><a href=""><img src="<?php /*echo the_field('paypal_logo','option')*/?>"></a></li>
                            </ul>
                        </div>-->
                        <div class="col-md-3 col-sm-4">
                            <div class="col-footer">
                                <h3 class="title">Contact us</h3>
                                <ul class="list-unstyled">
                                    <li class="details"><i class="fa fa-phone" aria-hidden="true"></i><span class="contact-label">Phone:</span><span class="number"><?php echo the_field('phone_number', 'option')?></span></li>
                                    <li class="details"><i class="fa fa-envelope" aria-hidden="true"></i><span class="contact-label">Email:</span><span class="email"><?php echo the_field('email_address', 'option')?></span></li>
                                </ul>
                                <div class="social-media-links">
                                    <h3 class="title">Stay connected</h3>
                                    <ul class="list-inline">
                                        <li><span class="icon-social-link-container"><a href="<?php echo the_field('facebook_link', 'option');?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></span></li>
                                        <li><span class="icon-social-link-container"><a href="<?php echo the_field('twitter_link', 'option');?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></span></li>
                                        <li><span class="icon-social-link-container"><a href="<?php echo the_field('youtube_link', 'option');?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></span></li>
                                        <li><span class="icon-social-link-container"><a href="<?php echo the_field('googleplus-link', 'option');?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="col-footer">
                                <h3 class="title">Legal</h3>
                                <?php wp_nav_menu( array(
                                    'menu' => 'Legal',
                                    'menu_id'        => '',
                                    'menu_class'     => 'list-unstyled',
                                    'container'      => ''
                                ) ); ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="col-footer">
                                <h3 class="title">Quick link</h3>
                                <?php wp_nav_menu( array(
                                    'menu' => 'Footer Quick Link',
                                    'menu_id'        => '',
                                    'menu_class'     => 'list-unstyled',
                                    'container'      => ''
                                ) ); ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="col-footer">
                                <h3 class="title">My account</h3>
                                <?php wp_nav_menu( array(
                                    'menu' => 'Footer My Account',
                                    'menu_id'        => '',
                                    'menu_class'     => 'list-unstyled',
                                    'container'      => ''
                                ) ); ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                           <div class="col-footer">
                               <h3 class="title">Shipping services</h3>
                               <a href=""><img src="<?php echo the_field('australia_post','option')?>"></a>
                               <ul class="list-inline">
                                   <li><a href=""><img src="<?php echo the_field('post_express_delivery','option')?>"></a></li>
                                   <li><a href=""><img src="<?php echo the_field('australia_map','option')?>"></a></li>
                                   <li><a href=""><img src="<?php echo the_field('secured_services','option')?>"></a></li>
                                   <li><a href=""><img src="<?php echo the_field('paypal_logo','option')?>"></a></li>
                               </ul>
                           </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p class="copyright-details text-center">© Copyright 2017. All Rights Reserved.   Website by Impressive Digital</p>

                        <div class="return-top-btn" id="toTop" title="Go to top"><i class="arrow-head-black-up"></i></div>
                    </div>
                </div>
            </div>
        </footer>
    </div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
