<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
$featured_img_url = get_the_post_thumbnail_url(get_queried_object_id(),'twentyseventeen-featured-image');
?>

<div class="single-featured-image-header" style="background-image:url(<?php echo esc_url($featured_img_url); ?>)">
    <div class="container">
        <div class="subpage-banner-content">
            <h2 class="subpage-banner-title"><?php the_field('subpages_title','188')?></h2>
            <?php custom_breadcrumbs(); ?>
        </div>
    </div>
</div><!-- .single-featured-image-header -->
<div class="blog-page">
    <div class="container">
        <div class="blog-container">
            <div class="post-categories-container">
                <ul class="list-inline">
                    <li><a href="/blog" id="all" <?php if($_GET['id'] == '') { ?> class='active' <?php } ?>>All</a></li>
                    <?php
                    $args = array(
                        'post_type' => 'post'
                    );
                    $categories = get_categories( $args );
                    $menuCatClass = '';
                    ?>

                    <?php foreach ( $categories as $category ) { ?>
                        <li>/</li>
                        <li><a href="/blog/?id=<?php echo $category->term_id; ?>" class="<?php if( $_GET['id'] == $category->term_id ) { echo'active';} ?>" ><?php echo $category->name; ?></a></li>
                    <?php } ?>
                </ul>
                <br>

            </div>
            <div class="blog-post-container">
                <div class="row">
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $category = $_GET["id"];
                    $args = array(
                        'post_type' => 'post',
                        'cat'       => $category,
                        'paged'     => $paged
                    );
                    ?>
                    <?php $myquery = new WP_Query($args);?>
                    <?php while ($myquery->have_posts()) : $myquery->the_post(); ?>
                        <div class="col-md-4 col-sm-6">
                            <div class="blog-post-item">
                                <div class="blog-post-image-container">
                                    <a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a>
                                </div>
                                <div class="blog-post-details">
                                    <h3 class="blog-post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                    <span class="blog-post-date"><?php the_date();?></span>
                                    <?php the_excerpt();?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile;?>
                </div>
                <!-- Add the pagination functions here. -->
                <div class="blog-post-navigation">
                    <?php
                    $current_page = max(1, get_query_var('paged'));
                    $total_pages = $myquery->max_num_pages;

                    $args = array(
                        'prev_text'  => __('<i class="fa fa-caret-left" aria-hidden="true"></i>'),
                        'next_text'  => __('<i class="fa fa-caret-right" aria-hidden="true"></i>'),
                        'current' => $current_page,
                        'total' => $total_pages
                        );
                    ?>
                    <?php echo paginate_links($args); ?>
                </div>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();
